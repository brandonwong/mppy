package matrix

import (
	"fmt"
)

func main() {
	fmt.Println(MatrixAdd(2.0, 3.0))
}

func MatrixAdd(a float32, b float32) float32 {
	return a + b
}